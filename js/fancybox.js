/*
 * FancyBox - jQuery Plugin
 * Simple and fancy lightbox alternative
 *
 * Examples and documentation at: http://fancybox.net
 *
 * Copyright (c) 2008 - 2010 Janis Skarnelis
 * That said, it is hardly a one-person project. Many people have submitted bugs, code, and offered their advice freely. Their support is greatly appreciated.
 *
 * Version: 1.3.4 (11/11/2010)
 * Requires: jQuery v1.3+
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
(function (a) {
    var p, u, v, e, B, m, C, j, y, z, s = 0, d = {}, q = [], r = 0, c = {}, k = [], E = null, n = new Image, H = /\.(jpg|gif|png|bmp|jpeg)(.*)?$/i, S = /[^\.]\.(swf)\s*$/i, I, J = 1, x = 0, w = "", t, g, l = !1, A = a.extend(a("<div/>")[0], {prop: 0}), K = navigator.userAgent.match(/msie [6]/i) && !window.XMLHttpRequest, L = function () {
        u.hide();
        n.onerror = n.onload = null;
        E && E.abort();
        p.empty()
    }, M = function () {
        !1 === d.onError(q, s, d) ? (u.hide(), l = !1) : (d.titleShow = !1, d.width = "auto", d.height = "auto", p.html('<p id="fancybox-error">The requested content cannot be loaded.<br />Please try again later.</p>'),
            D())
    }, G = function () {
        var b = q[s], c, f, e, g, k, j;
        L();
        d = a.extend({}, a.fn.fancybox.defaults, "undefined" == typeof a(b).data("fancybox") ? d : a(b).data("fancybox"));
        j = d.onStart(q, s, d);
        if (!1 === j)l = !1; else {
            "object" == typeof j && (d = a.extend(d, j));
            e = d.title || (b.nodeName ? a(b).attr("title") : b.title) || "";
            b.nodeName && !d.orig && (d.orig = a(b).children("img:first").length ? a(b).children("img:first") : a(b));
            "" === e && (d.orig && d.titleFromAlt) && (e = d.orig.attr("alt"));
            c = d.href || (b.nodeName ? a(b).attr("href") : b.href) || null;
            if (/^(?:javascript)/i.test(c) ||
                "#" == c)c = null;
            d.type ? (f = d.type, c || (c = d.content)) : d.content ? f = "html" : c && (f = c.match(H) ? "image" : c.match(S) ? "swf" : a(b).hasClass("iframe") ? "iframe" : 0 === c.indexOf("#") ? "inline" : "ajax");
            if (f)switch ("inline" == f && (b = c.substr(c.indexOf("#")), f = 0 < a(b).length ? "inline" : "ajax"), d.type = f, d.href = c, d.title = e, d.autoDimensions && ("html" == d.type || "inline" == d.type || "ajax" == d.type ? (d.width = "auto", d.height = "auto") : d.autoDimensions = !1), d.modal && (d.overlayShow = !0, d.hideOnOverlayClick = !1, d.hideOnContentClick = !1, d.enableEscapeButton = !1, d.showCloseButton = !1), d.padding = parseInt(d.padding, 10), d.margin = parseInt(d.margin, 10), p.css("padding", d.padding + d.margin), a(".fancybox-inline-tmp").unbind("fancybox-cancel").bind("fancybox-change", function () {
                a(this).replaceWith(m.children())
            }), f) {
                case "html":
                    p.html(d.content);
                    D();
                    break;
                case "inline":
                    if (!0 === a(b).parent().is("#fancybox-content")) {
                        l = !1;
                        break
                    }
                    a('<div class="fancybox-inline-tmp" />').hide().insertBefore(a(b)).bind("fancybox-cleanup",function () {
                        a(this).replaceWith(m.children())
                    }).bind("fancybox-cancel",
                        function () {
                            a(this).replaceWith(p.children())
                        });
                    a(b).appendTo(p);
                    D();
                    break;
                case "image":
                    l = !1;
                    a.fancybox.showActivity();
                    n = new Image;
                    n.onerror = function () {
                        M()
                    };
                    n.onload = function () {
                        l = !0;
                        n.onerror = n.onload = null;
                        d.width = n.width;
                        d.height = n.height;
                        a("<img />").attr({id: "fancybox-img", src: n.src, alt: d.title}).appendTo(p);
                        N()
                    };
                    n.src = c;
                    break;
                case "swf":
                    d.scrolling = "no";
                    g = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + d.width + '" height="' + d.height + '"><param name="movie" value="' + c + '"></param>';
                    k = "";
                    a.each(d.swf, function (a, b) {
                        g += '<param name="' + a + '" value="' + b + '"></param>';
                        k += " " + a + '="' + b + '"'
                    });
                    g += '<embed src="' + c + '" type="application/x-shockwave-flash" width="' + d.width + '" height="' + d.height + '"' + k + "></embed></object>";
                    p.html(g);
                    D();
                    break;
                case "ajax":
                    l = !1;
                    a.fancybox.showActivity();
                    d.ajax.win = d.ajax.success;
                    E = a.ajax(a.extend({}, d.ajax, {url: c, data: d.ajax.data || {}, error: function (a) {
                        0 < a.status && M()
                    }, success: function (a, b, f) {
                        if (200 == ("object" == typeof f ? f : E).status) {
                            if ("function" == typeof d.ajax.win) {
                                j =
                                    d.ajax.win(c, a, b, f);
                                if (!1 === j) {
                                    u.hide();
                                    return
                                }
                                if ("string" == typeof j || "object" == typeof j)a = j
                            }
                            p.html(a);
                            D()
                        }
                    }}));
                    break;
                case "iframe":
                    N()
            } else M()
        }
    }, D = function () {
        var b = d.width, c = d.height, b = -1 < b.toString().indexOf("%") ? parseInt((a(window).width() - 2 * d.margin) * parseFloat(b) / 100, 10) + "px" : "auto" == b ? "auto" : b + "px", c = -1 < c.toString().indexOf("%") ? parseInt((a(window).height() - 2 * d.margin) * parseFloat(c) / 100, 10) + "px" : "auto" == c ? "auto" : c + "px";
        p.wrapInner('<div style="width:' + b + ";height:" + c + ";overflow: " + ("auto" ==
            d.scrolling ? "auto" : "yes" == d.scrolling ? "scroll" : "hidden") + ';position:relative;"></div>');
        d.width = p.width();
        d.height = p.height();
        N()
    }, N = function () {
        var b, h;
        u.hide();
        if (e.is(":visible") && !1 === c.onCleanup(k, r, c))a.event.trigger("fancybox-cancel"), l = !1; else {
            l = !0;
            a(m.add(v)).unbind();
            a(window).unbind("resize.fb scroll.fb");
            a(document).unbind("keydown.fb");
            e.is(":visible") && "outside" !== c.titlePosition && e.css("height", e.height());
            k = q;
            r = s;
            c = d;
            if (c.overlayShow) {
                if (v.css({"background-color": c.overlayColor, opacity: c.overlayOpacity,
                    cursor: c.hideOnOverlayClick ? "pointer" : "auto", height: a(document).height()}), !v.is(":visible")) {
                    if (K)a("select:not(#fancybox-tmp select)").filter(function () {
                        return"hidden" !== this.style.visibility
                    }).css({visibility: "hidden"}).one("fancybox-cleanup", function () {
                        this.style.visibility = "inherit"
                    });
                    v.show()
                }
            } else v.hide();
            b = O();
            var f = {}, F = c.autoScale, n = 2 * c.padding;
            f.width = -1 < c.width.toString().indexOf("%") ? parseInt(b[0] * parseFloat(c.width) / 100, 10) : c.width + n;
            f.height = -1 < c.height.toString().indexOf("%") ? parseInt(b[1] *
                parseFloat(c.height) / 100, 10) : c.height + n;
            if (F && (f.width > b[0] || f.height > b[1]))"image" == d.type || "swf" == d.type ? (F = c.width / c.height, f.width > b[0] && (f.width = b[0], f.height = parseInt((f.width - n) / F + n, 10)), f.height > b[1] && (f.height = b[1], f.width = parseInt((f.height - n) * F + n, 10))) : (f.width = Math.min(f.width, b[0]), f.height = Math.min(f.height, b[1]));
            f.top = parseInt(Math.max(b[3] - 20, b[3] + 0.5 * (b[1] - f.height - 40)), 10);
            f.left = parseInt(Math.max(b[2] - 20, b[2] + 0.5 * (b[0] - f.width - 40)), 10);
            g = f;
            w = c.title || "";
            x = 0;
            j.empty().removeAttr("style").removeClass();
            if (!1 !== c.titleShow && (w = a.isFunction(c.titleFormat) ? c.titleFormat(w, k, r, c) : w && w.length ? "float" == c.titlePosition ? '<table id="fancybox-title-float-wrap" cellpadding="0" cellspacing="0"><tr><td id="fancybox-title-float-left"></td><td id="fancybox-title-float-main">' + w + '</td><td id="fancybox-title-float-right"></td></tr></table>' : '<div id="fancybox-title-' + c.titlePosition + '">' + w + "</div>" : !1) && "" !== w)switch (j.addClass("fancybox-title-" + c.titlePosition).html(w).appendTo("body").show(), c.titlePosition) {
                case "inside":
                    j.css({width: g.width - 2 * c.padding, marginLeft: c.padding, marginRight: c.padding});
                    x = j.outerHeight(!0);
                    j.appendTo(B);
                    g.height += x;
                    break;
                case "over":
                    j.css({marginLeft: c.padding, width: g.width - 2 * c.padding, bottom: c.padding}).appendTo(B);
                    break;
                case "float":
                    j.css("left", -1 * parseInt((j.width() - g.width - 40) / 2, 10)).appendTo(e);
                    break;
                default:
                    j.css({width: g.width - 2 * c.padding, paddingLeft: c.padding, paddingRight: c.padding}).appendTo(e)
            }
            j.hide();
            e.is(":visible") ? (a(C.add(y).add(z)).hide(), b = e.position(), t = {top: b.top, left: b.left, width: e.width(),
                height: e.height()}, h = t.width == g.width && t.height == g.height, m.fadeTo(c.changeFade, 0.3, function () {
                var b = function () {
                    m.html(p.contents()).fadeTo(c.changeFade, 1, P)
                };
                a.event.trigger("fancybox-change");
                m.empty().removeAttr("filter").css({"border-width": c.padding, width: g.width - 2 * c.padding, height: d.autoDimensions ? "auto" : g.height - x - 2 * c.padding});
                h ? b() : (A.prop = 0, a(A).animate({prop: 1}, {duration: c.changeSpeed, easing: c.easingChange, step: Q, complete: b}))
            })) : (e.removeAttr("style"), m.css("border-width", c.padding), "elastic" ==
                c.transitionIn ? (t = R(), m.html(p.contents()), e.show(), c.opacity && (g.opacity = 0), A.prop = 0, a(A).animate({prop: 1}, {duration: c.speedIn, easing: c.easingIn, step: Q, complete: P})) : ("inside" == c.titlePosition && 0 < x && j.show(), m.css({width: g.width - 2 * c.padding, height: d.autoDimensions ? "auto" : g.height - x - 2 * c.padding}).html(p.contents()), e.css(g).fadeIn("none" == c.transitionIn ? 0 : c.speedIn, P)))
        }
    }, P = function () {
        a.support.opacity || (m.get(0).style.removeAttribute("filter"), e.get(0).style.removeAttribute("filter"));
        d.autoDimensions &&
        m.css("height", "auto");
        e.css("height", "auto");
        w && w.length && j.show();
        c.showCloseButton && C.show();
        (c.enableEscapeButton || c.enableKeyboardNav) && a(document).bind("keydown.fb", function (b) {
            if (27 == b.keyCode && c.enableEscapeButton)b.preventDefault(), a.fancybox.close(); else if ((37 == b.keyCode || 39 == b.keyCode) && c.enableKeyboardNav && "INPUT" !== b.target.tagName && "TEXTAREA" !== b.target.tagName && "SELECT" !== b.target.tagName)b.preventDefault(), a.fancybox[37 == b.keyCode ? "prev" : "next"]()
        });
        c.showNavArrows ? ((c.cyclic && 1 <
            k.length || 0 !== r) && y.show(), (c.cyclic && 1 < k.length || r != k.length - 1) && z.show()) : (y.hide(), z.hide());
        c.hideOnContentClick && m.bind("click", a.fancybox.close);
        c.hideOnOverlayClick && v.bind("click", a.fancybox.close);
        a(window).bind("resize.fb", a.fancybox.resize);
        c.centerOnScroll && a(window).bind("scroll.fb", a.fancybox.center);
        "iframe" == c.type && a('<iframe id="fancybox-frame" name="fancybox-frame' + (new Date).getTime() + '" frameborder="0" hspace="0" ' + (navigator.userAgent.match(/msie [6]/i) ? 'allowtransparency="true""' :
            "") + ' scrolling="' + d.scrolling + '" src="' + c.href + '"></iframe>').appendTo(m);
        e.show();
        l = !1;
        a.fancybox.center();
        c.onComplete(k, r, c);
        var b, h;
        k.length - 1 > r && (b = k[r + 1].href, "undefined" !== typeof b && b.match(H) && (h = new Image, h.src = b));
        0 < r && (b = k[r - 1].href, "undefined" !== typeof b && b.match(H) && (h = new Image, h.src = b))
    }, Q = function (b) {
        var a = {width: parseInt(t.width + (g.width - t.width) * b, 10), height: parseInt(t.height + (g.height - t.height) * b, 10), top: parseInt(t.top + (g.top - t.top) * b, 10), left: parseInt(t.left + (g.left - t.left) * b,
            10)};
        "undefined" !== typeof g.opacity && (a.opacity = 0.5 > b ? 0.5 : b);
        e.css(a);
        m.css({width: a.width - 2 * c.padding, height: a.height - x * b - 2 * c.padding})
    }, O = function () {
        return[a(window).width() - 2 * c.margin, a(window).height() - 2 * c.margin, a(document).scrollLeft() + c.margin, a(document).scrollTop() + c.margin]
    }, R = function () {
        var b = d.orig ? a(d.orig) : !1, h = {};
        b && b.length ? (h = b.offset(), h.top += parseInt(b.css("paddingTop"), 10) || 0, h.left += parseInt(b.css("paddingLeft"), 10) || 0, h.top += parseInt(b.css("border-top-width"), 10) || 0, h.left +=
            parseInt(b.css("border-left-width"), 10) || 0, h.width = b.width(), h.height = b.height(), h = {width: h.width + 2 * c.padding, height: h.height + 2 * c.padding, top: h.top - c.padding - 20, left: h.left - c.padding - 20}) : (b = O(), h = {width: 2 * c.padding, height: 2 * c.padding, top: parseInt(b[3] + 0.5 * b[1], 10), left: parseInt(b[2] + 0.5 * b[0], 10)});
        return h
    }, T = function () {
        u.is(":visible") ? (a("div", u).css("top", -40 * J + "px"), J = (J + 1) % 12) : clearInterval(I)
    };
    a.fn.fancybox = function (b) {
        if (!a(this).length)return this;
        a(this).data("fancybox", a.extend({}, b, a.metadata ?
            a(this).metadata() : {})).unbind("click.fb").bind("click.fb", function (b) {
                b.preventDefault();
                l || (l = !0, a(this).blur(), q = [], s = 0, b = a(this).attr("rel") || "", !b || "" == b || "nofollow" === b ? q.push(this) : (q = a("a[rel=" + b + "], area[rel=" + b + "], img[rel=" + b + "]"), s = q.index(this)), G())
            });
        return this
    };
    a.fancybox = function (b, c) {
        var d;
        if (!l) {
            l = !0;
            d = "undefined" !== typeof c ? c : {};
            q = [];
            s = parseInt(d.index, 10) || 0;
            if (a.isArray(b)) {
                for (var e = 0, g = b.length; e < g; e++)"object" == typeof b[e] ? a(b[e]).data("fancybox", a.extend({}, d, b[e])) : b[e] =
                    a({}).data("fancybox", a.extend({content: b[e]}, d));
                q = jQuery.merge(q, b)
            } else"object" == typeof b ? a(b).data("fancybox", a.extend({}, d, b)) : b = a({}).data("fancybox", a.extend({content: b}, d)), q.push(b);
            if (s > q.length || 0 > s)s = 0;
            G()
        }
    };
    a.fancybox.showActivity = function () {
        clearInterval(I);
        u.show();
        I = setInterval(T, 66)
    };
    a.fancybox.hideActivity = function () {
        u.hide()
    };
    a.fancybox.next = function () {
        return a.fancybox.pos(r + 1)
    };
    a.fancybox.prev = function () {
        return a.fancybox.pos(r - 1)
    };
    a.fancybox.pos = function (b) {
        l || (b = parseInt(b),
            q = k, -1 < b && b < k.length ? (s = b, G()) : c.cyclic && 1 < k.length && (s = b >= k.length ? 0 : k.length - 1, G()))
    };
    a.fancybox.cancel = function () {
        l || (l = !0, a.event.trigger("fancybox-cancel"), L(), d.onCancel(q, s, d), l = !1)
    };
    a.fancybox.close = function () {
        function b() {
            v.fadeOut("fast");
            j.empty().hide();
            e.hide();
            a.event.trigger("fancybox-cleanup");
            m.empty();
            c.onClosed(k, r, c);
            k = d = [];
            r = s = 0;
            c = d = {};
            l = !1
        }

        if (!l && !e.is(":hidden"))if (l = !0, c && !1 === c.onCleanup(k, r, c))l = !1; else if (L(), a(C.add(y).add(z)).hide(), a(m.add(v)).unbind(), a(window).unbind("resize.fb scroll.fb"),
            a(document).unbind("keydown.fb"), m.find("iframe").attr("src", K && /^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank"), "inside" !== c.titlePosition && j.empty(), e.stop(), "elastic" == c.transitionOut) {
            t = R();
            var h = e.position();
            g = {top: h.top, left: h.left, width: e.width(), height: e.height()};
            c.opacity && (g.opacity = 1);
            j.empty().hide();
            A.prop = 1;
            a(A).animate({prop: 0}, {duration: c.speedOut, easing: c.easingOut, step: Q, complete: b})
        } else e.fadeOut("none" == c.transitionOut ? 0 : c.speedOut, b)
    };
    a.fancybox.resize =
        function () {
            v.is(":visible") && v.css("height", a(document).height());
            a.fancybox.center(!0)
        };
    a.fancybox.center = function (b) {
        var a, d;
        if (!l && (d = !0 === b ? 1 : 0, a = O(), d || !(e.width() > a[0] || e.height() > a[1])))e.stop().animate({top: parseInt(Math.max(a[3] - 20, a[3] + 0.5 * (a[1] - m.height() - 40) - c.padding)), left: parseInt(Math.max(a[2] - 20, a[2] + 0.5 * (a[0] - m.width() - 40) - c.padding))}, "number" == typeof b ? b : 200)
    };
    a.fancybox.init = function () {
        a("#fancybox-wrap").length || (a("body").append(p = a('<div id="fancybox-tmp"></div>'), u = a('<div id="fancybox-loading"><div></div></div>'),
            v = a('<div id="fancybox-overlay"></div>'), e = a('<div id="fancybox-wrap"></div>')), B = a('<div id="fancybox-outer"></div>').append('<div class="fancybox-bg" id="fancybox-bg-n"></div><div class="fancybox-bg" id="fancybox-bg-ne"></div><div class="fancybox-bg" id="fancybox-bg-e"></div><div class="fancybox-bg" id="fancybox-bg-se"></div><div class="fancybox-bg" id="fancybox-bg-s"></div><div class="fancybox-bg" id="fancybox-bg-sw"></div><div class="fancybox-bg" id="fancybox-bg-w"></div><div class="fancybox-bg" id="fancybox-bg-nw"></div>').appendTo(e),
            B.append(m = a('<div id="fancybox-content"></div>'), C = a('<a id="fancybox-close"></a>'), j = a('<div id="fancybox-title"></div>'), y = a('<a href="javascript:;" id="fancybox-left"><span class="fancy-ico" id="fancybox-left-ico"></span></a>'), z = a('<a href="javascript:;" id="fancybox-right"><span class="fancy-ico" id="fancybox-right-ico"></span></a>')), C.click(a.fancybox.close), u.click(a.fancybox.cancel), y.click(function (b) {
            b.preventDefault();
            a.fancybox.prev()
        }), z.click(function (b) {
            b.preventDefault();
            a.fancybox.next()
        }),
            a.fn.mousewheel && e.bind("mousewheel.fb", function (b, c) {
                if (l)b.preventDefault(); else if (0 == a(b.target).get(0).clientHeight || a(b.target).get(0).scrollHeight === a(b.target).get(0).clientHeight)b.preventDefault(), a.fancybox[0 < c ? "prev" : "next"]()
            }), a.support.opacity || e.addClass("fancybox-ie"), K && (u.addClass("fancybox-ie6"), e.addClass("fancybox-ie6"), a('<iframe id="fancybox-hide-sel-frame" src="' + (/^https/i.test(window.location.href || "") ? "javascript:void(false)" : "about:blank") + '" scrolling="no" border="0" frameborder="0" tabindex="-1"></iframe>').prependTo(B)))
    };
    a.fn.fancybox.defaults = {padding: 10, margin: 40, opacity: !1, modal: !1, cyclic: !1, scrolling: "auto", width: 560, height: 340, autoScale: !0, autoDimensions: !0, centerOnScroll: !1, ajax: {}, swf: {wmode: "transparent"}, hideOnOverlayClick: !0, hideOnContentClick: !1, overlayShow: !0, overlayOpacity: 0.7, overlayColor: "#777", titleShow: !0, titlePosition: "float", titleFormat: null, titleFromAlt: !1, transitionIn: "fade", transitionOut: "fade", speedIn: 300, speedOut: 300, changeSpeed: 300, changeFade: "fast", easingIn: "swing", easingOut: "swing", showCloseButton: !0,
        showNavArrows: !0, enableEscapeButton: !0, enableKeyboardNav: !0, onStart: function () {
        }, onCancel: function () {
        }, onComplete: function () {
        }, onCleanup: function () {
        }, onClosed: function () {
        }, onError: function () {
        }};
    a(document).ready(function () {
        a.fancybox.init()
    })
})(jQuery);

