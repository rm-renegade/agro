/* create an array of days which need to be disabled */
var disabledDays = [
    "11-23-2013",
    "11-25-2013",
    "11-26-2013",
    "12-2-2013",
    "12-4-2013",
    "12-5-2013",
    "12-7-2013",
    "12-9-2013",
    "12-11-2013",
    "12-15-2013",
    "12-18-2013",
    "12-19-2013",
    "12-22-2013",
    "12-27-2013",
    "12-28-2013",
    "12-29-2013",
    "12-31-2013",
    "1-1-2014",
    "1-2-2014",
    "1-4-2014",
    "1-5-2014",
    "1-9-2014",
    "1-13-2014",
    "1-15-2014",
    "1-18-2014"
];

/* utility functions */
function nationalDays(date) {
    var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
    //console.log('Checking (raw): ' + m + '-' + d + '-' + y);
    for (i = 0; i < disabledDays.length; i++) {
        if($.inArray((m+1) + '-' + d + '-' + y,disabledDays) != -1 || new Date() > date) {
            //console.log('bad:  ' + (m+1) + '-' + d + '-' + y + ' / ' + disabledDays[i]);
            return [false];
        }
    }
    //console.log('good:  ' + (m+1) + '-' + d + '-' + y);
    return [true];
}
function noWeekendsOrHolidays(date) {
    /*var noWeekend = jQuery.datepicker.noWeekends(date);
    return noWeekend[0] ? nationalDays(date) : noWeekend;*/

    var noWeekend = jQuery.datepicker.noWeekends(date);
    return nationalDays(date);
}
function datepickerBorderRadius() {

    var $table = $('.ui-datepicker-calendar');

    $table.find('tbody > tr:last-child').addClass('last-row');
    $table.find('th:first-child, td:first-child').addClass('first-col');
    $table.find('th:last-child, td:last-child').addClass('last-col');
}




$(document).ready(function(){

    /* create datepicker */
    $("#datepicker-range").datepicker({
        "dateFormat": "dd.mm.yy",
        "changeMonth": false,
        "changeYear": false,
        "numberOfMonths": 2,
        "showOtherMonths": true,
        "firstDay": 1,
        "dayNamesMin": [ "ВС", "ПН", "ВТ", "СР", "ЧТ", "ПТ", "СБ" ],
        "monthNames": [ "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        "beforeShowDay": noWeekendsOrHolidays,
        "minDate": '-1m',
        "maxDate": '2y',
        "onSelect": datepickerBorderRadius
    });

    /* call datepicker */
    $('.calendar').on('click', '.pseudolink', function(){

        var $calendar = $(this).parents('.calendar');
        var open = 'datepicker-open';
        var close = 'datepicker-close';

        var id = $(this).attr('id');
        switch (id) {
            case open:
                var newId = close;
                break;
            case close:
                var newId = open;
                break;
        }

        $(this).removeAttr('id').attr('id',newId);
        $calendar.find('.datepicker').slideToggle(300);

        datepickerBorderRadius();
    });
})



